#!/bin/bash

# You can call this script like this:
# $./brightness.sh up
# $./brightness.sh down

function get_brightness {
    xbacklight -get | cut -d '.' -f 1
}

function send_notification {
    DIR=`dirname "$0"`
    brightness=`get_brightness`
    # Make the bar with the special character ─ (it's not dash -)
    # https://en.wikipedia.org/wiki/Box-drawing_character
#bar=$(seq -s "─" $(($brightness/5)) | sed 's/[0-9]//g')
icon_name="/usr/share/icons/Faba/48x48/notifications/notification-display-brightness.svg"
if [ "$brightness" = "0" ]; then
$DIR/notify-send.sh "$brightness""      " -i "$icon_name" -t 2000 -h int:value:"$brightness" -h string:synchronous:"─" --replace=555
fi

bar=$(seq -s "─" $(($brightness/5)) | sed 's/[0-9]//g')
# Send the notification
$DIR/notify-send.sh "$brightness""     ""$bar" -i "$icon_name" -t 2000 -h int:value:"$brightness" -h string:synchronous:"$bar" --replace=555

}

case $1 in
    up)
	xbacklight -inc 10
	send_notification
	;;
    down)
	xbacklight -dec 10
	send_notification
	;;
esac

