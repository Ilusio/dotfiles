sudo pacman -S --noconfirm git fzf sddm qt5 ttf-fira-mono 
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ..
rm -rf yay
yay -Syu --nocleanmenu --nodiffmenu antibody gitflow-avh polybar ttf-fira-mono rofi nerd-fonts-source-code-pro flat-remix alsa-utils faba-icon-theme devmon nerd-fonts-hack nerd-fonts-fira-code

#sddm
sudo mv sddm/clairvoyance /usr/share/sddm/themes/clairvoyance
sudo mkdir -p /etc/sddm.conf.d/
sudo mv sddm/sddm.conf /etc/sddm.conf.d/

#refind
sudo mkdir -p /boot/EFI/refind/themes
sudo mv ursamajor-rEFInd /boot/EFI/refind/themes/
sudo zsh -c 'echo "include themes/ursamajor-rEFInd/theme.conf" >> /boot/EFI/refind/refind.conf'
sudo zsh -c 'echo "resolution 1600 900" >> /boot/EFI/refind/refind.conf'

chmod +x .scripts/volume.sh .scripts/notify-send.sh .scripts/powermenu.sh .scripts/brightness.sh .script/lockscreen/lock.sh
mv .p10k.zsh .zshrc  .zsh_plugins.txt .scripts ~
mv Wallpaper.jpg ~/Pictures

cp -r .config ~
rm -rf .config
